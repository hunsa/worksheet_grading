{ lib, python3Packages }:
with python3Packages;
buildPythonApplication {
  pname = "worksheet_grading";
  version = "1.4.2";

  propagatedBuildInputs = [ 
    readchar
    colorama
    pandas
  ];

  src = ./.;
}
