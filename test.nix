with import <nixpkgs> {};
with pkgs.python3Packages;

buildPythonPackage rec {
  name = "worksheet-grading";
  src = ./.;
  propagatedBuildInputs = [ pandas readchar colorama ];
}