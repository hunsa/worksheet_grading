__author__ = "Klaus Kraßnitzer"
__copyright__ = "Copyright (C) 2022 " + __author__
__version__ = "1.4.2"
__license__ = "Distributed under the MIT License: <https://mit-license.org/>"
__program__ = "Worksheet Grading Script"
__package__ = "worksheet-grading"
__repository__ = "https://gitlab.com/kdvkrs/worksheet_grading"
