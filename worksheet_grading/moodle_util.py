"""
Utility constants for Moodle's grading worksheet CSV format.
"""
class MoodleCSV:
    NO_GROUP = "Not a member of any group, so unable to make submissions."
    FULL_NAME = "Full name"
    LAST_MODIFIED_SUB = "Last modified (submission)"
    NOT_MODIFIED = "-"
    GROUP = "Group"
    MAX_GRADE = "Maximum Grade"
    GRADE = "Grade"
    FEEDBACK_COMMENTS = "Feedback comments"
    LAST_MODIFIED_GRADE = "Last modified (grade)"
    DATE_FORMAT = "%A, %d %b %Y, %I:%M %p"